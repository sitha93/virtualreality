#ifndef MYCOLORVISUALIZER_H
#define MYCOLORVISUALIZER_H

//gmlib
#include <scene/gmvisualizer.h>

class MyColorVisualizer : public GMlib::Visualizer
{
public:
    MyColorVisualizer();

    virtual ~MyColorVisualizer();

    const GMlib::Color&      getColor() const;
    void              setColor( const GMlib::Color& color );

protected:


};

#endif // MYCOLORVISUALIZER_H
